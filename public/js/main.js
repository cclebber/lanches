var app = angular.module('app', ['ngRoute', 'ngResource']);


app.config(function($routeProvider, $locationProvider) {
  $routeProvider    
  .when('/', {
    templateUrl: 'views/cardapio.html',
    controller: 'CardapioController'
  })
  .when('/lanche/:id', {
    templateUrl: 'views/lanche.html',
    controller: 'LancheController'
  })
  $locationProvider.html5Mode(true);
});

app.factory('api', function($resource) {
  return $resource('http://localhost:3000/lanches/:id',{id: '@id'},
  {
    query : {
      method: 'GET'
    },
    put : {
      method: 'PUT'
    }
  });
});




app.controller('Main', ['$scope', function(sx){
    
  console.info('haha');



}]);

app.controller('CardapioController', ['$scope', 'api', function(sx, api){

    api.query({}, function(ret) {
      console.info('ret',ret);
      sx.lanches=ret.lanches;
    });

}]);

app.controller('LancheController', ['$scope', 'api','$routeParams', function(sx, api, rp){    
  
  api.get({id:rp.id}).$promise.then(function(ret) {
    // console.info('ret', ret);
    sx.lanche=ret.lanche;
    calculoPrecos();
  })


  sx.adicionaQuantidade=function(ingrediente)
  {
    ingrediente.quantidade++;
    calculoPrecos();
  }

  sx.removeQuantidade=function(ingrediente)
  {
    if(ingrediente.quantidade) ingrediente.quantidade--;
    calculoPrecos();
  }

  function calculoPrecos() {

    api.put({id:'atualiza', ingredientes:sx.lanche.lista}).$promise.then(function(ret) {
      console.info('ret', ret);
      sx.lanche.preco=ret.preco;
      sx.lanche.promocoes=ret.promocoes;
    })
    
  }




}]);