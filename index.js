var express = require('express');
var path = require('path');
var app = express();

app.use(express.static('public'));

app.use('/', function(req, res){
    res.sendFile('views/home.html', {
        root: path.join(__dirname, './')
    })
})

app.listen(4000, function () {

    console.log('ligado na porta 4000');

});